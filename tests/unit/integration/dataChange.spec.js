import { mount } from '@vue/test-utils';
import App from  '@/App.vue';

const  wrapper = mount(App)
const  vm = wrapper.vm

const nameTag = wrapper.find('.pokemon-name')

    const lastNameValue = nameTag.text()

    const imgTag = wrapper.find('img')

    const lastImgValue = imgTag.attributes().src

    const typeTag = wrapper.find('#type')

    const lastTypeValue = typeTag.text()

    const weightTag = wrapper.find('#weight')

    const lastWeightValue = weightTag.text()

    const heightTag = wrapper.find('#height')

    const lastHeightValue = heightTag.text()

    const abilitiesTag = wrapper.find('ul')

    const lastAbilitiesValue = abilitiesTag.text()

describe('Probar que se cambió la propiedad changeTest ', () => {

    const btn = wrapper.find('button')

    btn.trigger('click')

    test('should change name', async () => {

        

        expect(nameTag.text()).not.toContain('name')

        await wrapper.vm.$nextTick()

        .then(()=>{expect(nameTag.text()).not.toBe(lastNameValue)})

    

    })
})
